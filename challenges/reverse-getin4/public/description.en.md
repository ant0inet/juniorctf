# Getin 4

Category: Reverse

** Solve Getin 3 before you try this one **

Hi dude! Try and crack this program `getin4`.

You run it on Linux: 

```
$ ./getin4
=== I am the gate keeper ===
Password: 
```

Type the correct password to get the `ACCESS GRANTED` message.

## How do I do that?

I recommend you use Radare2: `r2 ./getin4`
This is mainly like getin3, but the algorithm is different.


