# Moyenne

Pico a eu ses notes de sieste. Il a eu:

- 19/20
- 19/20
- 17/20
- 11/20 (quel pignouf !)
- 17/20
- 18/20

Il faudrait calculer sa moyenne. Tu sais que la moyenne, c'est la *somme* de toutes les notes (18, 19 ...) divisé par le *nombre de notes*.

Il faudrait faire un programme en C qui calcule la **moyenne** de Pico.

Pour marquer les points, tu devras rentrer **la valeur de la moyenne** affichée par ton programme.

## Où commencer

Un programme doit savoir où *commencer*. Cela s'appelle souvent le *main* en programmation. En C, cela se déclare ainsi:

```C
void main() {
     /* ici tu ecris ton programme */
}
```

Tu écriras ton programme là où j'ai mis "ici tu ecris ton programme".

Au passage, note qu'un commentaire commence par `/*` et terminer par `*/`. Tu peux mettre autant de commentaires que tu veux dans ton code. Ils permettent de mieux relire et comprendre ton code quand tu le reliras dans quelques jours...

## Tableau 

Non ! Je ne parle pas d'un tableau d'artiste ! Mais imagine une sorte de boite avec des cases.

On va mettre les notes dans un **tableau d'entiers**.

### Avez-vous quelque chose à déclarer ?

Pour cela, il faut *déclarer* le tableau, c'est à dire, dire dans le programme "je vais utiliser un tableau d'entiers".
Cela se déclare comme ca:

```C
int tableau[NBELEM];
```

`NBELEM` est le nombre d'éléments à mettre dans le tableau. A toi de remplacer par 4, 5, 6 ou le chiffre que tu veux.

### Y a quoi dans les cases ?

Pour accéder à chaque case du tableau, tu mets:

- `tableau[0]`: première case du tableau
- `tableau[1]`: deuxième case du tableau
- `tableau[2]`: troisième case du tableau
- ...

Attention, note bien que la première case est à **zéro**. 
Aussi, on ne peut pas aller voir une case du tableau qui n'existe pas. Par exemple, si tu as défini 5 cases dans ton tableau, alors tu ne peux pas aller voir `tableau[10]`.

Pour remplir une case du tableau, tu fais:

```C
tableau[0] = 10;
```

Ceci met la valeur 10 dans la première case du tableau.

## Variable

Une **variable** c'est une donnée à laquelle tu donnes un nom. Dans le paragraphe précédent, par exemple, `tableau` est une variable. Tu aurais tout aussi bien pu l'appeler `petDeMammouth`.

Généralement, on utilise des variables qui aident à comprendre ce que le programme fait, meme si `petDeMammouth` a surement beaucoup de succès...

```C
int tarte_aux_pommes;
int temperature;
int notes;
int chocolat;
int petDeMammouth;
```

## Calculs

### Addition

Pour additionner deux nombres, on utiliser... devine ? On utilise `+` !! 

```C
i = 2 + 4;
```
Quelle valeur se trouve dans `i` ? Ben 6 pardi ! Andouille !!


Tu peux aussi additionner des variables. Cela revient à additionner le contenu des variables.

```C
int i = 1;
int j = 2;
int z = i +j +3;
```

Dans cet exemple, z vaut 6.

### Division 

Les **divisions** sont un peu plus compliquées parce que quand tu divises, tu n'as pas forcément un résultat entier.

Dans ce cas là, le résultat doit etre déclaré comme `float` et pas `int`.

```C
float moyenne;
```

Le symbole de la division est `/`. Si chaque membre de la division est entier (mais le résultat ne l'est pas forcément), il faut forcer l'ordinateur à travailler avec des nombres décimaux. Voilà comment tu fais:

```C
float moyenne;
moyenne = (float) somme / nb;
```

## Afficher 

Pour afficher à l'écran quelque chose, par exemple le résultat de ta moyenne, il faut utiliser `printf`.

Pour afficher un entier, tu feras:

```C
printf("Valeur: %d\n", tableau[2]);
```

- Tu peux remplacer `Valeur` par ce que tu veux.
- Le `%d` est important et veut dire "met une valeur entiere ici".
- Le `\n` veut dire de passer à la ligne suivante. Ce n'est pas obligatoire de le mettre, mais alors tout sera écrit à la suite.


`printf` est une *fonction* toute faite (c'est à dire quelqu'un a écrit le programme pour toi qui va dire à l'ordinateur d'afficher des choses à l'écran). Pour pouvoir l'utiliser, tu dois mettre au début de ton programme (exactement comme ça):

```C
#include <stdio.h>
```

Si tu veux afficher un nombre décimal, il faut que tu utilises `%f` à la place de `%d`

## Les petits trucs casse-pieds du C


### Point virgule ?

- Toutes les instructions doivent terminer par un point virgule `;`. Par exemple:

```
`C
tableau[0] = 10;
```


Note que dans certains cas, tu ne mets pas de ; par exemple ici:

```C
void main() { /* pas de ; */
     i = 2;
} /* pas de ; */
```


### Minuscule ou majuscule ?

En programmation, les minuscules ou les majuscules ont de l'importance. 
La variable `Pingouin` est différente de la variable `pingouin`.

```C
int pingouin = 2;
int Pingouin = 5;
```

### Espaces

Dans les noms de variables, tu ne peux pas mettre d'espaces. 

Tu peux mettre des `_`, mais pas de `-`. 
D'une manière générale, reste simple et **ne mets pas d'accents ni de ponctuation**.

Dans ton programme en revanche, tu peux mettre des espaces (presque) où tu veux:

```C
void main() {
     

     i = 4;
}
```
C'est pareil que:

```C
void main() {
     i = 4;
}
```
Et encore pareil que (mais on évite la version ci dessous):

```C
void           main() {
     i =        4;
}
```

## Compiler

Ton programme n'est pas compréhensible pour un ordinateur.
Il faut le traduire. Pour cela tu vas taper:

```
gcc prog.c -o prog
```

- prog.c: c'est le nom de fichier C. Si tu l'as appelé autrement, change le nom !
- prog: c'est le nom final du fichier que l'ordinateur comprend. Là aussi, comme presque partout en informatique, tu peux changer le nom par autre chose.

Ensuite, pour éxecuter le programme, tu taperas:

```
./prog
```

Si tout se passe bien, ça marchera, sinon, tu auras des messages d'erreur.
S'il y a des erreurs, ce n'est grave ! Ca arrive à **tous les programmeurs**. Il faut juste essayer de comprendre le message. Il te guide un peu normalement...

**Tu sais tout, à toi !**


