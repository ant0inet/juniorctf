#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) {
  char line[25];
  char pass[] = "Penguin";

  printf("=== This is a secret base ===\n");
  printf("Password: ");
  
  if (fgets(line, sizeof(line), stdin) !=NULL) {
    line[strcspn(line, "\n")]= 0;
    if (strcmp(line, pass) == 0 ) {
      printf("\n ACCESS GRANTED !!!\n");
      return 0;
    }
  }

  printf("Huh. Go back to nursery school!\n");
  return -1;
}
