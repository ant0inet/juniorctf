# John 1

Palpaguin vient de mettre au point un systeme de controle de l'Etoile Noire.

Des rebelles ont reussi (au prix de leur vie) à brièvement avoir accès à ce système. Ils ont eu le temps de copier un fichier important qui contient le mot de passe haché de Palpaguin. Mais ils n'ont pas eu le temps de trouver exactement le mot de passe. 

Ils savent juste que le mot de passe de palpaguin est un *fruit* tout en minuscule. 

Peux-tu nous aider et te logger sur la machine ? 
Tu disposes du fichier de mot de passe haché (à télécharger).

## Accéder au système de controle

Pour accéder au système de controle de l'Etoile Noire, tape: `ssh -p 2022 palpaguin@192.168.0.8`

Explication: 

- ssh est un vrai protocole qu'on utilise très souvent.
- `-p 2022` spécifie le *port* de communication à utiliser. C'est comme le numéro d'une porte, mais sur ordinateur.
- `palpaguin` c'est le nom d'utilisateur de palpaguin
- `192.168.0.8` c'est l'adresse du serveur


Si cela te dit quelque chose comme ceci, répond oui (yes):
```
The authenticity of host '[127.0.0.1]:2022 ([127.0.0.1]:2022)' can't be established.
RSA key fingerprint is 2d:93:f1:87:f8:91:8f:87:46:63:72:fd:c9:24:ed:d9.
Are you sure you want to continue connecting (yes/no)?
```

Ensuite, le systeme te demande un mot de passe. C'est ce mot de passe que l'on veut que tu trouves.

```
palpaguin@192.168.0.8's password: 
```

## Attention

Ceci est un jeu. Dans la vraie vie, il est interdit de casser des mots de passe. Tu ne dois jamais le faire (meme pour rire ou épater la galerie).

On peut cependant parfois en avoir besoin lorsqu'il s'agit de *sa propre machine* et qu'on a oublié le mot de passe. Accéder à sa propre machine est bien entendu permis !

## Casser des mots de passe avec John

Pour y arriver:

1. Installe l'outil *John The Ripper* (Jacque l'Eventreur) en tapant dans un terminal: `sudo apt-get install john`
2. Ecris dans un fichier une liste de fruits que tu connais. Un fruit par ligne. Sauvegarde ce fichier.
3. John The Ripper peut tester les mots de passe possibles pour toi. Pour cela, dans un terminal, lance `john --wordlist=tonfichier mdphache`

