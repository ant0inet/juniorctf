# Les voitures de Pico

*Il faut résoudre l'énigme du garage de Pico avant d'aborder celui ci*

Bravo ! Tu es entré dans le garage de Pico. Et là, peut etre as-tu remarqué que ... tu ne pouvais pas utiliser ses voitures, parce qu'il fallait un *autre* mot de passe ! Damned !

Emprunte une voiture de Pico. Peu importe laquelle.

## Emprunter une voiture

Pour accéder à une voiture, il te faut:

- le *login* de la voiture, c'est à dire son nom d'utilisateur. Comme pico pour Pico, ou boris pour Boris. Sauf que là, c'est un nom de voiture. 

- le *mot de passe* de cette voiture. Ca, tu le trouveras avec John The Ripper.

## Trouver le login

Entre dans le garage de Pico. Tu y accèdes via SSH:

- port: `2822`
- utilisateur: `pico`
- machine: `ADRESSE-IP-MACHINE`
- mot de passe de Pico: tu as trouvé ça dans l'énigme du garage de Pico

**Une fois que tu es dans le garage de Pico**, tu peux trouver les **logins** des voitures en lancant la commande suivante:

```
cat /etc/passwd | cut -d: -f1 | tail -n 3
```

## Les mots de passe possibles

On sait que le mot de passe des voitures de Pico est basé sur des parties de voitures. Par exemple, un pneu, c'est une partie de voiture, donc ça pourrait etre quelque chose comme ça.

Ecris une liste de parties de voiture. Ecris la en **minuscules**, **sans accents**.

Tu dois mettre cette liste de parties de voiture quelque part dans le garage de Pico. Par exemple, dans le compte de pico ici `/home/pico/parties`. 

## Attention, il y a deux machines: la tienne et le garage de Pico

Attention dans cette énigme tu dois comprendre qu'il y a deux machines différentes:

- la tienne
- le garage de Pico (tu vas dessus via SSH)

Les fichiers ne vont pas d'une machine à l'autre par magie ! Si tu veux écrire un fichier dans le garage de Pico, tu dois etre dans le garage de Pico. Ou copier le fichier vers le garage de Pico (on verra comment plus tard).


## Je ne te donne pas mdpchiffre !

A toi cette fois ci de le récupérer sur la machine.
Pour cela, **quand tu es dans le garage de Pico**, lance la commande suivante:

```
unshadow /etc/passwd /etc/shadow | tail -n 3 > /tmp/lesvoitures
```

Cette commande fabrique le fichier `mdpchiffre`, sauf que cette fois ci il s'appelle `/tmp/lesvoitures`

Si tu veux, tu peux lire le contenu de `/tmp/lesvoitures`

## Fichier de configuration

Le fichier de configuration `john.conf` est déjà en place dans le garage de Pico. Ne t'en préoccupe pas et utilise celui la.

## Lancer John The Ripper

Dans le garage de Pico, quand tu as:

- la liste de parties de voiture
- le fichier de configuration de John The Ripper au bon endroit
- le fichier `/tmp/lesvoitures` à craquer

Tu es pret à lancer John The Ripper.

```
john --wordlist=/home/pico/parties --rules /tmp/lesvoitures
```

Si tu as de la chance, tu trouveras des mots de passe !


## Entrer dans une voiture

Quand tu as le mot de passe d'une voiture, entre dedans via SSH:

- port: `2822`
- utilisateur: le login de la voiture
- machine: `ADRESSE-IP-MACHINE`
- mot de passe: le mot de passe que tu as trouvé

Tu trouveras ainsi le drapeau.
**Pas besoin d'entrer dans chaque voiture (sauf si ça te fait plaisir). Une seule suffit pour trouver le drapeau et valider l'épreuve.**





