## Requirements - current limitations

Your kids will need access to:

- SSH client
- a terminal
- an editor
- John The Ripper (I'll try and remove that requirement later)
- Internet. Several challenges don't strictly require Internet, but it might be helpful to find some documentation.

Current limitation: challenges are in **French** currently. But I'm sure it won't be too difficult to translate! 

## Setting up your own Junior CTF

To setup your own Junior CTF, you need:

- **This repository**. It contains the scenarios & sources for the challenges.
- A **CTF framework** to validate the flags, handle scores. And perhaps hints, teams etc. I personally use [CTFd](https://github.com/CTFd/CTFd). I'm sure you can get it to work with something else.
- **Docker**. Each challenge runs in its own Docker container.

The steps are the following:

1. Setup CTFd. Create an admin account.
2. Clone this repository. `git clone https://framagit.org/axellec/juniorctf.git`
3. `cd juniorctf`
4. `make build LANG=fr`. This builds the Docker images for each challenge. You only need to do this once.
5. `make up`. This starts a Docker container for each challenge. To stop the containers, simple do `make down`
6. **Manually create each challenge on CTFd**. Yes, I'm afraid currently this is manual. It's nothing difficult to do though (mostly copy/paste). This means logging in as CTF administrator, then creating each challenge. For each challenge, you copy the description which is located in  each `./CHALLENGENAME/public/description.LANG.md` (where LANG suits your language), upload needed files if any (they will be in `./CHALLENGENAME/public`), set the number of points for the challenge and enter the flag (provided `./CHALLENGENAME/FLAG`).

