# How to contribute?

## What you can contribute to

*If you want to contribute, you are welcome!*

Tech:

- Provide an import file for CTFd containing all challenges
- Ensure that each kid gets his/her own Docker container, and not a container shared (and possibly "polluted") by other kids...
- Modify Docker image of john 1-5 so that participants don't need to have John The Ripper on their own machine.
- Make this Windows friendly...
- Create a Docker image of Junior CTF?
- Add more challenges :)



Not tech:

- Translate challenges to English, and other languages (**started**)
- Host an instance of Junior CTF
- Art / images for challenges

Most challenges have a French version. Current status of translations:

| Challenges | Languages |
| ---------- | --------- |
| hydra1-frigo | fr, en |
| unix1-boris | fr, started en |
| getin 1 to 5 | en |


## How to add a new challenge

Each challenge has its own directory with:

- `Dockerfile`. To build the Docker image for this challenge. Try and keep the containers **light**, because there are several challenges! If your challenge doesn't need any container, then, you don't need it :) **Note: to support various languages, I am starting to rename Dockerfile to Dockerfile.fr or Dockerfile.en**
- `Makefile`. **Mandatory**. Must have target `build` - which will build the Docker image (if you don't need a Docker container, then just make this target do nothing). Other typical targets: `run` spawns a Docker container, `clean` stops and removes the container.
- `FLAG`. **Mandatory**. This file contains the expected flag - that's all. You don't want to have to re-do all challenges to find the flag, I tell you. Even if they are easy. **Note: to support various languages, I am starting to rename FLAG to FLAG.xx where xx is the 2 letter language code, because some flags are in the given language**
- `solution.md`. Optional. Explains how to solve this challenge.
- `./public` directory. **Mandatory**. Place in this directory all information you need to provide to the kids to solve the challenge. Make sure not to put anything secret in here!
- `./public/description.fr.md`. **Mandatory**. Describes the scenario for the kids to read. `fr` for French - change for another language.
- Place in `./public` any other file you need to provide. The executable to reverse engineer, an unshadowed password file, images etc.
- The challenge directory (`.`) may contain other files, for example source files used to generate an executable, tests etc. Freely organize the rest of the directory the way that suits you!


